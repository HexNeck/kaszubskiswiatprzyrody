var WIN = false;
var numerSlowa = 0;
var znalezione = [];
var word = setWord();
var COUNT = 0;
var TUTORIAL = 0;
var notFind = true;
gf.preload();

function init() {
    gf.loadImage('./images/background.png', 989, 508, "#obrazek");
    dodawanieTabelki();
    draw();
    $("#aboutUs").hide();

};

//ustawia nowe słowo
function setWord() {
    var wordToGuess = slownik.objects[numerSlowa].slowo;
    return wordToGuess;
};

//sprawdza czy nie znalazłeś wcześniej tego znaku
function verifiChar(char) {
    var nieMaGo = 0;
    if (znalezione.length == 0) {
        findChar(char);
    } else {
        for (var i = 0; i < znalezione.length; i++) {
            if (char == znalezione[i]) {
                break;
            } else {
                nieMaGo++;
            }
        }
    }
    if (nieMaGo == znalezione.length) {
        findChar(char);
    }
};

function findChar(char) {
    var znak = podzielSlowo(word);
    var findedNumber = word.length;
    for (var i = 0; i < word.length; i++) {
        if (char == znak[i]) {
            flip(i);
            $("#back" + i).html('<span class="table-text">' + char + "</span>");
            znalezione.push(char);
            notFind = false;
            checkWin();
            addWinStatus(char);
        } else {
            findedNumber--;
            if (findedNumber == 0) {
                notFind = true;
                feedBack(char);
            }
        }
    }
};

function podzielSlowo() {
    var znak = {};
    for (var i = 0; i < word.length; i++) {
        znak[i] = word.charAt(i);
    };
    return znak;
};
$(window).on('keydown', function (event) {
    if (TUTORIAL == 0) {
        if (event.altKey) {
            var klawiatura = String.fromCharCode(event.keyCode);
            var malaKlawiatura = klawiatura.toLowerCase();
            switch (malaKlawiatura) {
            case 'e':
                verifiChar('ę');
                break;
            case 'a':
                verifiChar('ą');
                break;
            case 's':
                verifiChar('ś');
                break;
            case 'n':
                verifiChar('ń');
                break;
            case 'l':
                verifiChar('ł');
                break;
            case 'o':
                verifiChar('ó');
                break;
            case 'z':
                verifiChar('ż');
                break;
            case 'x':
                verifiChar('ź');
                break;
            default:
                break;
            }
        } else if (!event.altKey) {
            var klawiatura = String.fromCharCode(event.keyCode);
            var malaKlawiatura = klawiatura.toLowerCase();
            if (malaKlawiatura == 08) {} else {
                verifiChar(malaKlawiatura);
            }
        }
    } else {
        var klawiatura = String.fromCharCode(event.keyCode);
        var malaKlawiatura = klawiatura.toLowerCase();
        checkWordTutorial(malaKlawiatura);
    }
});

function feedBack(char) {
    $("#status").html('');
    $("#status").append('Nacisnąłeś \'' + char + '\' niestety nie ma tej literki w tym słowie');
};

//dodaje tabelke
function dodawanieTabelki() {
    $("#tablica").append('<table id="tabelka"><tr>');
    for (var i = 0; i < word.length; i++) {
        $("#tabelka").append('<td class="table-content"><div id="card' + i + '" class="card"><div class = "front" > </div> <div id = "back' + i + '" class="back"></div></div></td >');
        //dodanie obracania się karty:
        $("#card" + i).flip({
            axis: 'y',
            trigger: 'manual'
        });
    }
    $("#tablica").append('</tr></table>');

};


function oneMore() {
    if (numerSlowa == slownik.objects.length - 1) {
        numerSlowa = 0;
        reset();
    } else {
        numerSlowa++;
        reset();
    }
    TUTORIAL = 0;
};

function oneLess() {
    if (numerSlowa == 0) {
        numerSlowa = slownik.objects.length - 1;
        reset();
    } else {
        numerSlowa--;
        reset();
    }
    TUTORIAL = 0;
};

function deleteTable() {
    $("#tablica").html("");
};

function reset() {
    deleteTable();
    word = setWord();
    deletedraw();
    draw();
    podzielSlowo();
    dodawanieTabelki();
    znalezione = [];
    checkWin();
    addWinStatus();
};


function checkWin() {
    if (znalezione.length == word.length) {
        WIN = true;
    } else {
        WIN = false;
    }
};

function addWinStatus(char) {
    if (WIN == true) {
        $("#status").html("");
        $("#status").append("Zgadłeś słowo! <br />Gratëlëjã!");
    } else {
        $("#status").html("");
    }
};

function draw() {
    var path = images[numerSlowa];
    $('#zwierze').width(128).height(128).append(path);
    gf.playSound(numerSlowa);

};

function deletedraw() {
    $('#zwierze').html('');
    //kasowanie dźwięku
    $('audio').stop();
    $('#sound').html('');
};

function play() {
    WIN = false;
    numerSlowa = 0;
    znalezione = [];
    word = setWord();
    COUNT = 0;
    TUTORIAL = 0;
    deleteTable();
    word = setWord();
    deletedraw();
    draw();
    podzielSlowo();
    dodawanieTabelki();
    znalezione = [];
    checkWin();
    addWinStatus();
    $("#game").show();
    $("#aboutUs").hide();
};

function replaySound() {
    $('audio').stop();
    $('#sound').html('');
    gf.playSound(numerSlowa);
};

function flip(i) {
    $('#card' + i).flip(true);
};

gf.rotation();