var slownik = {
    'objects': [
        {
            'slowo': 'bòcón',
            'url': './images/bocian.png',
            'urlSound': './audio/bocian.mp3'
           },
        {
            'slowo': 'kóń',
            'url': './images/kon.png',
            'urlSound': './audio/kon.mp3'
           },
        {
            'slowo': 'kòt',
            'url': './images/kot.png',
            'urlSound': './audio/kot.mp3'
           },
        {
            'slowo': 'tósz',
            'url': './images/pies.png',
            'urlSound': './audio/pies.mp3'
           },
        {
            'slowo': 'òséł',
            'url': './images/osiolek.png',
            'urlSound': './audio/osiolek.mp3'
           },
        {
            'slowo': 'mësz',
            'url': './images/mysz.png',
            'urlSound': './audio/mysz.mp3'
           },
        {
            'slowo': 'trus',
            'url': './images/krolik.png',
            'urlSound': './audio/krolik.mp3'
           },
        {
            'slowo': 'zgrzébiã',
            'url': './images/zrebak.png',
            'urlSound': './audio/zrebak.mp3'
           },
        {
            'slowo': 'mùczka',
            'url': './images/krowa.png',
            'urlSound': './audio/krowa.mp3'
           },
        {
            'slowo': 'òwca',
            'url': './images/owca.png',
            'urlSound': './audio/owca.mp3'
           },
        {
            'slowo': 'gãs',
            'url': './images/ges.png',
            'urlSound': './audio/ges.mp3'
           },
        {
            'slowo': 'kùra',
            'url': './images/kura.png',
            'urlSound': './audio/kura.mp3'
           },
        {
            'slowo': 'gùlôk',
            'url': './images/indyk.png',
            'urlSound': './audio/indyk.mp3'
           },
        {
            'slowo': 'kòza',
            'url': './images/koza.png',
            'urlSound': './audio/koza.mp3'
           },
        {
            'slowo': 'kaczka',
            'url': './images/kaczka.png',
            'urlSound': './audio/kaczka.mp3'
           },
        {
            'slowo': 'swiniô',
            'url': './images/swinka.png',
            'urlSound': './audio/swinka.mp3'
           },
        {
            'slowo': 'celôk',
            'url': './images/cielak.png',
            'urlSound': './audio/cielak.mp3'
           },
        {
            'slowo': 'kùr',
            'url': './images/kogut.png',
            'urlSound': './audio/kogut.mp3'
           },
        {
            'slowo': 'kùrczã',
            'url': './images/kurczak.png',
            'urlSound': './audio/kurczak.mp3'
           },
        {
            'slowo': 'jaje',
            'url': './images/jajko.png',
            'urlSound': './audio/jajko.mp3'
           },
        {
            'slowo': 'żaba',
            'url': './images/zaba.png',
            'urlSound': './audio/zaba.mp3'
           },
        {
            'slowo': 'bùla',
            'url': './images/byk.png',
            'urlSound': './audio/byk.mp3'
           },
        {
            'slowo': 'marchwia',
            'url': './images/marchewka.png',
            'urlSound': './audio/marchewka.mp3'
           },
        {
            'slowo': 'bùlwa',
            'url': './images/ziemniak.png',
            'urlSound': './audio/ziemniak.mp3'
           },
        {
            'slowo': 'czwikła',
            'url': './images/burak.png',
            'urlSound': './audio/burak.mp3'
           },
        {
            'slowo': 'cëbùla',
            'url': './images/cebula.png',
            'urlSound': './audio/cebula.mp3'
           },
        {
            'slowo': 'gùrka',
            'url': './images/ogorek.png',
            'urlSound': './audio/ogorek.mp3'
           },
        {
            'slowo': 'pòtrôwnica',
            'url': './images/truskawka.png',
            'urlSound': './audio/truskawka.mp3'
           },
        {
            'slowo': 'jabkò',
            'url': './images/jablko.png',
            'urlSound': './audio/jablko.mp3'
           },
        {
            'slowo': 'krëszka',
            'url': './images/gruszka.png',
            'urlSound': './audio/gruszka.mp3'
           },
        {
            'slowo': 'bania',
            'url': './images/dynia.png',
            'urlSound': './audio/dynia.mp3'
           },
        {
            'slowo': 'kùkùridza',
            'url': './images/kukurydza.png',
            'urlSound': './audio/kukurydza.mp3'
           },
        {
            'slowo': 'sliwka',
            'url': './images/sliwka.png',
            'urlSound': './audio/sliwka.mp3'
           },
        {
            'slowo': 'pòmidora',
            'url': './images/pomidor.png',
            'urlSound': './audio/pomidor.mp3'
           },
        {
            'slowo': 'mòrela',
            'url': './images/morela.png',
            'urlSound': './audio/morela.mp3'
           },
        {
            'slowo': 'groszk',
            'url': './images/groszek.png',
            'urlSound': './audio/groszek.mp3'
           },
        {
            'slowo': 'malëna',
            'url': './images/malina.png',
            'urlSound': './audio/malina.mp3'
           },
        {
            'slowo': 'wiszniô',
            'url': './images/wisnia.png',
            'urlSound': './audio/wisnia.mp3'
           },
        {
            'slowo': 'lës',
            'url': './images/lis.png',
            'urlSound': './audio/lis.mp3'
           },
        {
            'slowo': 'jéż',
            'url': './images/jez.png',
            'urlSound': './audio/jez.mp3'
           },
        {
            'slowo': 'wieszczówka',
            'url': './images/wiewiorka.png',
            'urlSound': './audio/wiewiorka.mp3'
           },
        {
            'slowo': 'łaniô',
            'url': './images/lania.png',
            'urlSound': './audio/lania.mp3'
           },
        {
            'slowo': 'jeléń',
            'url': './images/jelen.png',
            'urlSound': './audio/jelen.mp3'
           },
        {
            'slowo': 'wilk',
            'url': './images/wilk.png',
            'urlSound': './audio/wilk.mp3'
           },
        {
            'slowo': 'miedwiédz',
            'url': './images/niedzwiedz.png',
            'urlSound': './audio/niedzwiedz.mp3'
           },
        {
            'slowo': 'bòber',
            'url': './images/bobr.png',
            'urlSound': './audio/bobr.mp3'
           },
        {
            'slowo': 'mrówka',
            'url': './images/mrowka.png',
            'urlSound': './audio/mrowka.mp3'
           },
        {
            'slowo': 'bòrowiczka',
            'url': './images/biedronka.png',
            'urlSound': './audio/biedronka.mp3'
           },
        {
            'slowo': 'skòczk',
            'url': './images/konik-polny.png',
            'urlSound': './audio/konik-polny.mp3'
           },
        {
            'slowo': 'gòłąbk',
            'url': './images/golab.png',
            'urlSound': './audio/golab.mp3'
           }
           ],
    'getObject': function (slowo) {
        var result = null;
        for (var i = 0; i <= slownik.objects.length; i++) {
            var obj = slownik.objects.length[i];
            if (obj.slowo == slowo) {
                result = obj;
                break;
            }
        }
        return result;
    }
}