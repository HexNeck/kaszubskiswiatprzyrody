function tutorialInit() {
    TUTORIAL = 1;
    numerSlowa = 6;
    COUNT = 0;
    reset();
    word = setWord();
    podzielSlowo();
    checkWordTutorial();
    var literaDoZnalezienia;

};

function checkWordTutorial(malaKlawiatura) {
    if (COUNT == 0 && TUTORIAL == 1) {
        $("#status").append('Proszę o wciśnięcie przycisku T');
        literaDoZnalezienia = 't';
        COUNT++;
    } else if (COUNT == 1 && malaKlawiatura == 't' && TUTORIAL == 1) {
        $("#status").append('Teraz wciśnij R');
        verifiChar('t');
        literaDoZnalezienia = 'r';
        flip(0);
        COUNT++;
    } else if (COUNT == 2 && malaKlawiatura == 'r' && TUTORIAL == 1) {
        verifiChar('r');
        $("#status").append('teras wciśnij U');
        literaDoZnalezienia = 'u';
        flip(1);
        COUNT++;
    } else if (COUNT == 3 && malaKlawiatura == 'u' && TUTORIAL == 1) {
        verifiChar('u');
        $("#status").append('teras wciśnij S');
        literaDoZnalezienia = 's';
        flip(1);
        COUNT++;
    } else if (COUNT == 4 && malaKlawiatura == 's' && TUTORIAL == 1) {
        verifiChar('s');
        $("#status").append('Świetnie! Napisałeś całe słowo! Królik po Kaszubsku to Trus!<br />');
        TUTORIAL = 0;
        flip(2);
    } else {
        $("#status").html('');
        $("#status").append('Nacisnąłeś \'' + malaKlawiatura + '\'poszukaj ' + literaDoZnalezienia + ' na klawiaturze');
        goDown();
    }
}

function goDown() {
    var n = $("#status").height();
    $('#status').scrollTop($('#status')[0].scrollHeight);
}
