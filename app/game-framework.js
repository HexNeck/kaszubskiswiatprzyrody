//load obrazka
var images = [];
var sounds = [];
gf = {};

//do usunięcia jeżeli zacznie działać
gf.loadImage = function (path, width, height, target) {
    $('<img src="' + path + '">').load(function () {
        $(this).width(width).height(height).appendTo(target);
    });
};
//do tego miejsca
gf.imagesToPreload = [];
gf.soundToPreload = []; // dodane do dźwięków

gf.preload = function () {
    $(document).ready(function () {
            $("#aboutUs").hide();
        })
        //w pliku proba.js zobacz ile jest elementów w objects
    var doPobrania = slownik.objects.length;
    //dodaj wszystkie do gf.imagesToPreload
    for (i = 0; i < doPobrania; i++) {
        var url = slownik.objects[i].url;
        var urlSound = slownik.objects[i].urlSound;
        gf.imagesToPreload.push(url);
        gf.soundToPreload.push(urlSound); // dodane do dźwięków
    };
    // pętla która pobiera
    for (var i = 0; i < doPobrania; i++) {
        var image = new Image();
        images.push(image);
        image.src = gf.imagesToPreload[i];
    };
    // pętla która pobiera dźwięki
    for (var i = 0; i < doPobrania; i++) {
        var sound = new Audio();
        sounds.push(sound);
        sound.src = gf.soundToPreload[i];
    }

    //utwórz pętle która sprawdza pobieranie
    var preloadingPoller = setInterval(function () {
        var counter = 0;
        var total = gf.imagesToPreload.length;
        for (var i = 0; i < total; i++) {
            if (images[i].complete) {
                counter++;
                progressCallBack(counter, total);
            }
        }
        if (counter == total) {
            clearInterval(preloadingPoller);
            endCallback();
        }

        //		else{
        // tego nie kumam
        //			if (progressCallback){
        //				count++;
        //				progressCallback((count / total)* 100);
        //			}
        //		}
    }, 100);
};

gf.odkryj = function () {
    $('#plansza').show();
};

//Gdy dojdzie do 100%
function endCallback() {
    gf.odkryj();
    init();
};

//gdy się załaduje kolejny obrazek
function progressCallBack(liczba, total) {
    $("#progress-bar").width(liczba * 2 + "%");
    $("#progress-bar").html(liczba * 2 + "%")
    if (liczba == total) {
        //zniknij pasek podstępu
        $('#progress').hide();
    }
};

//granie dźwięków
gf.playSound = function (numerSlowa) {
    $("#sound").append(sounds[numerSlowa]);
    $("audio").attr('id', 'dzwiek');
    $("audio").attr('type', 'audio/mpeg');
    var dzwiek = document.getElementById("dzwiek");
    dzwiek.play();
}

//rotacja

gf.rotation = function () {
    var interval = null;
    var counter = 0;
    interval = setInterval(function () {
        if (counter > -360) {
            counter -= 1;
            $('#airscrew').css({
                MozTransform: 'rotate(-' + -counter + 'deg)',
                WebkitTransform: 'rotate(' + -counter + 'deg)',
                transform: 'rotate(' + -counter + 'deg)'
            });
        } else {
            counter = 0
        }
    }, 10);
}